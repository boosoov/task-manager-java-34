package com.rencredit.jschool.boruak.taskmanager.listener.task;

import com.rencredit.jschool.boruak.taskmanager.endpoint.DeniedAccessException_Exception;
import com.rencredit.jschool.boruak.taskmanager.endpoint.EmptyUserIdException_Exception;
import com.rencredit.jschool.boruak.taskmanager.endpoint.SessionDTO;
import com.rencredit.jschool.boruak.taskmanager.endpoint.TaskEndpoint;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.event.ConsoleEvent;
import com.rencredit.jschool.boruak.taskmanager.listener.AbstractListener;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class TaskListClearListener extends AbstractListener {

    @Autowired
    private TaskEndpoint taskEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all tasks.";
    }

    @Override
    @EventListener(condition = "@taskListClearListener.name() == #event.command")
    public void handle(final ConsoleEvent event)throws EmptyUserIdException_Exception, DeniedAccessException_Exception {
        System.out.println("[CLEAR TASKS]");

        @NotNull final SessionDTO session = systemObjectService.getSession();
        taskEndpoint.removeAllUserTasks(session);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}
