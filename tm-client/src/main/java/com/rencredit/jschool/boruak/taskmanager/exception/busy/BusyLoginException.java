package com.rencredit.jschool.boruak.taskmanager.exception.busy;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractClientException;

public class BusyLoginException extends AbstractClientException {

    public BusyLoginException() {
        super("Error! Login is busy...");
    }

}
