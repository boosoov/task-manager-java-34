package com.rencredit.jschool.boruak.taskmanager.listener.data;

import com.rencredit.jschool.boruak.taskmanager.endpoint.*;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.event.ConsoleEvent;
import com.rencredit.jschool.boruak.taskmanager.listener.AbstractListener;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class DataJsonLoadListener extends AbstractListener {

    @Autowired
    private AdminEndpoint adminEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-json-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from json file";
    }

    @Override
    @EventListener(condition = "@dataJsonLoadListener.name() == #event.command")
    public void handle(final ConsoleEvent event)throws IOException_Exception, NotExistUserException_Exception, EmptyRoleException_Exception, DeniedAccessException_Exception, EmptySessionException_Exception, EmptyUserIdException_Exception, EmptyElementsException_Exception, EmptyIdException_Exception, EmptyHashLineException_Exception, EmptyUserException_Exception, EmptyLoginException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception {
        System.out.println("[DATA JSON LOAD]");
        @NotNull final SessionDTO session = systemObjectService.getSession();
        adminEndpoint.loadJson(session);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
