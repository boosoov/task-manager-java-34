package com.rencredit.jschool.boruak.taskmanager.exception.empty;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractClientException;

public class EmptyEmailException extends AbstractClientException {

    public EmptyEmailException() {
        super("Error! Email is empty...");
    }

}
