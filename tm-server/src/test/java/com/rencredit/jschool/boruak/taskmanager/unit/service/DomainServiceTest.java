package com.rencredit.jschool.boruak.taskmanager.unit.service;

import com.rencredit.jschool.boruak.taskmanager.marker.UnitTestCategory;
import org.junit.experimental.categories.Category;

@Category(UnitTestCategory.class)
public class DomainServiceTest {
//
//    @NotNull final EntityManagerFactoryUtil entityManagerFactoryUtil = new EntityManagerFactoryUtil(new PropertyService());
//    @NotNull private IProjectService projectService;
//    @NotNull private ITaskService taskService;
//    @NotNull private IUserService userService;
//    @NotNull private IDomainService domainService;
//
//    @Before
//    public void init() throws EmptyLoginException, EmptyHashLineException, BusyLoginException, EmptyRoleException, EmptyPasswordException, EmptyUserException, DeniedAccessException {
//        projectService = new ProjectService();
//        taskService = new TaskService();
//        userService = new UserService();
//        domainService = new DomainService(projectService, taskService, userService);
//
//        userService.clearAll();
//    }
//
//    @Test
//    public void testExport() throws EmptyNameException, EmptyUserIdException, EmptyPasswordException, BusyLoginException, EmptyHashLineException, EmptyLoginException, EmptyUserException, DeniedAccessException, EmptyElementsException {
//        @Nullable final UserDTO user = new UserDTO("login123", "password");
//        @Nullable final ProjectDTO project = new ProjectDTO(user.getId(), "nameProject");
//        @Nullable final TaskDTO task = new TaskDTO(user.getId(), "nameTask", "description");
//
//        userService.add("login123", user);
//        projectService.merge(project);
//        taskService.merge(task);
//
//        @NotNull final Domain domain = new Domain();
//        Assert.assertTrue(domain.getUsers().isEmpty());
//        Assert.assertTrue(domain.getTasks().isEmpty());
//        Assert.assertTrue(domain.getProjects().isEmpty());
//
//        domainService.export(domain);
//        Assert.assertFalse(domain.getUsers().isEmpty());
//        Assert.assertFalse(domain.getTasks().isEmpty());
//        Assert.assertFalse(domain.getProjects().isEmpty());
//    }
//
//    @Test
//    public void testLoad() throws EmptyElementsException, EmptyLoginException, EmptyNameException, EmptyUserIdException, EmptyIdException, EmptyPasswordException, EmptyRoleException, BusyLoginException, EmptyHashLineException, EmptyUserException, DeniedAccessException {
//        @Nullable final UserDTO user = new UserDTO("login123", "password");
//        @Nullable final ProjectDTO project = new ProjectDTO(user.getId(), "nameProject");
//        @Nullable final TaskDTO task = new TaskDTO(user.getId(), "nameTask", "description");
//
//        Assert.assertNull(userService.getById(user.getId()));
//        Assert.assertNull(projectService.findOneEntityById(user.getId(), project.getId()));
//        Assert.assertNull(taskService.findOneById(user.getId(), task.getId()));
//
//        @NotNull final Domain domain = new Domain();
//        domain.setUsers(Arrays.asList(user));
//        domain.setProjects(Arrays.asList(project));
//        domain.setTasks(Arrays.asList(task));
//        domainService.load(domain);
//
//        Assert.assertNotNull(userService.getById(user.getId()));
//        Assert.assertNotNull(projectService.findOneEntityById(user.getId(), project.getId()));
//        Assert.assertNotNull(taskService.findOneById(user.getId(), task.getId()));
//    }

}
