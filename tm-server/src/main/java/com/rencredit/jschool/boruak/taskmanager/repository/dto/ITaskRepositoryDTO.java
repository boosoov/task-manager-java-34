package com.rencredit.jschool.boruak.taskmanager.repository.dto;

import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ITaskRepositoryDTO extends IAbstractRepositoryDTO<TaskDTO> {

    @NotNull
    List<TaskDTO> findAllByUserId(@NotNull String userId);

    @NotNull
    List<TaskDTO> findAllByProjectId(@NotNull String projectId);

    @Nullable
    TaskDTO findByUserIdAndName(@NotNull String userId, @NotNull String name);

}
