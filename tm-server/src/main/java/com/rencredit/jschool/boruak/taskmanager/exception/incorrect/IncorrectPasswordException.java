package com.rencredit.jschool.boruak.taskmanager.exception.incorrect;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractException;

public class IncorrectPasswordException extends AbstractException {

    public IncorrectPasswordException() {
        super("Error! Password incorrect...");
    }

}
