package com.rencredit.jschool.boruak.taskmanager.exception.logic;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractException;

public class InternalProgramException extends AbstractException {

    public InternalProgramException() {
        super("Internal program logic exception...");
    }

}
