package com.rencredit.jschool.boruak.taskmanager.exception.incorrect;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractException;

public class IncorrectIndexException extends AbstractException {

    public IncorrectIndexException(final Throwable cause) {
        super(cause);
    }

    public IncorrectIndexException(final String value) {
        super("Error! This value ``" + value + "`` is not number...");
    }

    public IncorrectIndexException() {
        super("Error! Index is incorrect...");
    }

}
