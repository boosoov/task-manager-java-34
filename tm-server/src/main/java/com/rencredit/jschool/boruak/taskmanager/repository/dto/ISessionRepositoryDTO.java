package com.rencredit.jschool.boruak.taskmanager.repository.dto;

import com.rencredit.jschool.boruak.taskmanager.dto.SessionDTO;
import org.springframework.stereotype.Repository;

@Repository
public interface ISessionRepositoryDTO extends IAbstractRepositoryDTO<SessionDTO> {

}
