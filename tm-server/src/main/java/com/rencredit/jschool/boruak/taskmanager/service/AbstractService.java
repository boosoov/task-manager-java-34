package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.api.service.IService;
import com.rencredit.jschool.boruak.taskmanager.dto.AbstractDTO;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.notexist.NotExistAbstractListException;
import com.rencredit.jschool.boruak.taskmanager.repository.entity.IAbstractRepositoryEntity;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

public class AbstractService<E extends AbstractDTO> implements IService<E> {

    private IAbstractRepositoryEntity repository;

    @Override
    @Transactional
    public void load(@Nullable final Collection<E> elements) throws EmptyElementsException {
        if (elements == null || elements.isEmpty()) throw new EmptyElementsException();
        repository.saveAll(elements);
    }

    @Override
    @Transactional
    public boolean merge(@Nullable final E element) throws EmptyElementsException {
        if (element == null) throw new EmptyElementsException();
        repository.save(element);
        return true;
    }

    @Override
    @Transactional
    public void merge(@Nullable final Collection<E> elements) throws NotExistAbstractListException {
        if (elements == null) throw new NotExistAbstractListException();
        repository.saveAll(elements);
    }

    @Override
    @Transactional
    public void merge(@Nullable final E... elements) throws NotExistAbstractListException {
        if (elements == null || elements.length == 0) throw new NotExistAbstractListException();
        repository.save(elements);
    }

    @Override
    @Transactional
    public void deleteAll() throws EmptyPasswordException, BusyLoginException, EmptyHashLineException, EmptyLoginException, EmptyUserException, DeniedAccessException, EmptyRoleException {
        repository.deleteAll();;
    }

}
